import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Card } from 'antd';

const FormItem = Form.Item;

class UserProfile extends React.Component {
  // handleSubmit = e => {
  //   e.preventDefault();
  //   this.props.form.validateFields((err, values) => {
  //     if (!err) {
  //       console.log('Received values of form: ', values);
  //     }
  //   });
  // };
  render() {
    // const { getFieldDecorator } = this.props.form;
    return (
      <div style={{ display: 'flex' }}>
        <div style={{ width: 600, paddingTop: 70, paddingLeft: 300 }}>
          <Card style={{ width: 1000, height: 550 }}>
            <FormItem>
              <h1 style={{ textAlign: 'center' }}>User Profile</h1>
            </FormItem>

            <FormItem style={{ paddingLeft: 100 }}>
              <h2>Name: </h2>
              <br />
              <h2>E-mail: </h2>
              <br />
              <h2>Tel: </h2>
              <br />
              <h2>Position: </h2>
              <br />
              <h2>Role: </h2>
              <br />
            </FormItem>
          </Card>
        </div>
      </div>
    );
  }
}
export default UserProfile;
