import React from 'react';
import { Menu, Icon } from 'antd';
import { Link, withRouter } from 'react-router-dom';
class Navbar extends React.Component {
  state = {
    current: 'home',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };

  render() {
    return (
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="horizontal"
        align="right"
      >
        <Menu.Item key="home">
          <Link to="/">
            <Icon type="home" />
            Home
          </Link>
        </Menu.Item>

        <Menu.Item key="mail">
          <Link to="login">
            <Icon type="mail" />
            Login
          </Link>
        </Menu.Item>

        <Menu.Item key="app">
          <Link to="register">
            <Icon type="user-add" />
            Register
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default withRouter(Navbar);
