import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Card } from 'antd';

const FormItem = Form.Item;

class LoginForm extends React.Component {
  // handleSubmit = e => {
  //   e.preventDefault();
  //   this.props.form.validateFields((err, values) => {
  //     if (!err) {
  //       console.log('Received values of form: ', values);
  //     }
  //   });
  // };
  render() {
    // const { getFieldDecorator } = this.props.form;
    return (
      <div style={{ display: 'flex' }}>
        <div style={{ width: 400, margin: 'auto', paddingTop: 100 }}>
          <Card style={{ width: 500 }}>
            <FormItem>
              <h1 style={{ textAlign: 'center' }}>SignIn</h1>
            </FormItem>

            <FormItem>
              <Input
                prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                placeholder="E-Mail"
              />
            </FormItem>
            <FormItem>
              <Input
                prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                type="password"
                placeholder="Password"
              />
            </FormItem>
            <FormItem>{<Checkbox>Remember me</Checkbox>}</FormItem>
            <FormItem>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Log in
              </Button>
              Or <a href="/Register">register now!</a>
            </FormItem>
          </Card>
        </div>
      </div>
    );
  }
}
export default LoginForm;
