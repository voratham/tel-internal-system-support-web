import React from 'react';
import { Form, Input, Button, Card, Select } from 'antd';

const { Option } = Select;
const FormItem = Form.Item;

class Register extends React.Component {
  state = {
    confirmDirty: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '66',
    })(
      <Select style={{ width: 70 }}>
        <Option value="66">+66</Option>
      </Select>
    );

    return (
      <div style={{ display: 'flex' }}>
        <div style={{ width: 400, margin: 'auto', paddingTop: 100 }}>
          <Card style={{ width: 500 }}>
            <Form onSubmit={this.handleSubmit}>
              <FormItem>
                <h1 style={{ textAlign: 'center' }}>SignUp</h1>
              </FormItem>
              <Form.Item {...formItemLayout} label="Name">
                {getFieldDecorator('name', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input your Name!',
                    },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item {...formItemLayout} label="Username">
                {getFieldDecorator('username', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input your Username!',
                    },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item {...formItemLayout} label="E-mail">
                {getFieldDecorator('email', {
                  rules: [
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please input your E-mail!',
                    },
                  ],
                })(<Input />)}
              </Form.Item>

              <Form.Item {...formItemLayout} label="Password">
                {getFieldDecorator('password', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                    {
                      validator: this.validateToNextPassword,
                    },
                  ],
                })(<Input type="password" />)}
              </Form.Item>

              <Form.Item {...formItemLayout} label="Confirm Password">
                {getFieldDecorator('confirm', {
                  rules: [
                    {
                      required: true,
                      message: 'Please confirm your password!',
                    },
                    {
                      validator: this.compareToFirstPassword,
                    },
                  ],
                })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
              </Form.Item>

              <Form.Item {...formItemLayout} label="Phone Number">
                {getFieldDecorator('phone', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input your phone number!',
                    },
                  ],
                })(
                  <Input
                    addonBefore={prefixSelector}
                    style={{ width: '100%' }}
                  />
                )}
              </Form.Item>

              <FormItem {...tailFormItemLayout} style={{ textAlign: 'center' }}>
                <Button type="primary" htmlType="submit">
                  Register
                </Button>
              </FormItem>
            </Form>
          </Card>
        </div>
      </div>
    );
  }
}
export default Form.create()(Register);
