import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Landing from './components/Landing';
import Navbar from './components/Navbar';
import LoginForm from './components/LoginForm';
import WrappedRegistrationForm from './components/Register';
import Approve from './components/Approve';
import UserProfile from './components/UserProfile';
class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Route exact path="/" component={Landing} />
          <div className="container">
            <Route exact path="/login" component={LoginForm} />
            <Route exact path="/register" component={WrappedRegistrationForm} />
            <Route exact path="/approve" component={Approve} />
            <Route exact path="/userprofile" component={UserProfile} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
